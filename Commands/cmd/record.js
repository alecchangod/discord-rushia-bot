const { Constants } = require("discord.js");
const fs = require("fs");
const prism = require("prism-media");
const { spawn } = require("node:child_process");
const split = require("../../function/common/split.js");

module.exports = {
  name: "record",
  aliases: ["rec"],
  trans: "music",
  run: async (client, message, args, secret, prefix, trans) => {
    // Get translate
    var invalid_ch = trans.strings.find((it) => it.name === "invalid_ch").trans;
    var no_ch = trans.strings.find((it) => it.name === "no_ch").trans;
    let _;

    let voiceChannel;
    if (args[0]) {
      voiceChannel = await client.channels.fetch(args[0]);
      if (!Constants.VoiceBasedChannelTypes.includes(voiceChannel?.type)) {
        return message.channel.send(
          `${client.emotes.error} | ${args[0]} ${invalid_ch}`
        );
      }
    } else if (message.member.voice.channel.id) {
      voiceChannel = await client.channels.fetch(
        message.member.voice.channel.id
      );
    }

    if (!voiceChannel) {
      return message.channel.send(`${client.emotes.error} | ${no_ch}`);
    }

    // console.log(voiceChannel.name);

    let connection = await client.distube.voices.join(voiceChannel);
    await connection.setSelfDeaf(false);
    connection = connection.connection;

    const userStreams = new Map();
    let filename, path;
    connection.receiver.speaking.on("start", async (userId) => {
      if (userStreams.has(userId)) return;
      const user = await voiceChannel.guild.members.fetch(userId);
      let uname = user.nickname
        ? user.nickname
        : user.user.globalName
        ? user.user.globalName
        : user.user.username;
      split(
        `<@${userId}> (${uname}) started speaking`,
        message.channel,
        _,
        _,
        true
      );
      const receiver = connection.receiver.subscribe(userId);
      path = `record/${message.guild.id}/${voiceChannel.id}/raw`;
      filename = `${userId}(${uname})_${Date.now()}`;
      if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true });
      }

      const outStream = fs.createWriteStream(`${path}/${filename}.pcm`);
      userStreams.set(userId, { outStream, receiver });

      receiver
        .pipe(
          new prism.opus.Decoder({ rate: 48000, channels: 2, frameSize: 960 })
        )
        .pipe(outStream);
    });

    // detect disconnections
    client.on("voiceStateUpdate", (oldState, newState) => {
      // Check for user disconnection
      if (oldState.channelId && !newState.channelId) {
        // User disconnected from any voice channel
        let userId = oldState.id;

        // Check if the bot was recording this user's voice
        if (userStreams.has(userId)) {
          let streamData = userStreams.get(userId);

          streamData.receiver.destroy();
          streamData.outStream.end(async () => {
            await processfile(
              userId,
              userStreams,
              path,
              filename,
              message.channel,
              message.guild,
              "user"
            );
          });
        }
      }

      // if the bot itself has been disconnected
      if (
        oldState.id === client.user.id &&
        oldState.channelId &&
        !newState.channelId
      ) {
        // clean up all streams
        for (let [userId, streamData] of userStreams) {
          streamData.receiver.destroy();
          streamData.outStream.end(async () => {
            await processfile(
              userId,
              userStreams,
              path,
              filename,
              message.channel,
              message.guild,
              "bot"
            );
          });
        }
        userStreams.clear();
      }
    });
  },
};

async function processfile(
  userId,
  userStreams,
  path,
  filename,
  channel,
  guild,
  status
) {
  let _;
  let str = `Stream for user <@${userId}> (${userId}) has been ended due to ${status} disconnection.\n`;

  userStreams.delete(userId);

  const ffmpeg = spawn("ffmpeg", [
    "-f",
    "s16le",
    "-ar",
    "48k",
    "-ac",
    "2",
    "-i",
    `${path}/${filename}.pcm`,
    `${path}/../${filename}.m4a`,
  ]);

  ffmpeg.on("close", async (code) => {
    // console.log(`ffmpeg process exited with code ${code}`);
    if (code === 0) {
      str += "Done converting to .m4a file.\n";
      let m4afile = `${path}/../${filename}.m4a`;
      const filesize = fs.statSync(m4afile).size;
      if (filesize > sizelimit(guild)) {
        m4afile = null;
        const link = await upload(secret, m4afile);
        str += link;
      }
      split(str, channel, [m4afile], _, true);
    } else {
      str += `Failed converting to .m4a file.\n`;
      let pcmfile = `${path}/${filename}.pcm`;
      const filesize = fs.statSync(pcmfile).size;
      if (filesize > sizelimit(guild)) {
        pcmfile = null;
        const link = await upload(secret, pcmfile);
        str += link;
      }
      split(str, channel, [pcmfile], _, true);
    }
  });
}

const sizelimit = (guild) => {
  switch (guild.premiumTier) {
    case 3:
      return 104857600;
    case 2:
      return 52428800;
    default:
      return 26214400;
  }
};
