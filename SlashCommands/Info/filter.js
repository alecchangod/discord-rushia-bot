const {
  PermissionsBitField,
  ApplicationCommandOptionType,
  ButtonStyle,
} = require("discord.js");
const fs = require("fs");
const fetch = require("node-fetch");
const trans = require("../../function/common/trans.js");
const { QuickDB } = require("quick.db");
const db = new QuickDB({ filePath: "database/mod.sqlite" });
const split = require("../../function/common/split.js");

module.exports = {
  data: {
    name: "filter",
    description: "detect words in a group and give responses (case sensitive)",
    options: [
      {
        name: "add",
        type: ApplicationCommandOptionType.Subcommand,
        description: "add a filter",
        options: [
          {
            name: "filter_type",
            type: ApplicationCommandOptionType.String,
            description: "The type of message to filter",
            required: true,
            choices: [
              { name: "equal_text", value: "text" },
              { name: "include_text", value: "i_text" },
              { name: "attachments", value: "attach" },
              { name: "sticker_id", value: "sticker" },
            ],
          },
          {
            name: "respond_type",
            type: ApplicationCommandOptionType.String,
            description: "how to respond to the message(reply?)",
            required: true,
            choices: [
              { name: "reply", value: "reply" },
              { name: "message", value: "message" },
            ],
          },
          {
            name: "filter",
            type: ApplicationCommandOptionType.String,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "channel_id",
            type: ApplicationCommandOptionType.Channel,
            description:
              "Only filter in a specific channel/category (ignore this if you want it to work for the whole group)",
            required: false,
          },
          {
            name: "user_id",
            type: ApplicationCommandOptionType.User,
            description:
              "Only filter for a specific user (ignore this if you want it to work for everyone)",
            required: false,
          },
          {
            name: "role_id",
            type: ApplicationCommandOptionType.Role,
            description:
              "Only filter for users with specific role (ignore this if you want it to work for everyone)",
            required: false,
          },
          {
            name: "respond_text",
            type: ApplicationCommandOptionType.String,
            description: "The text to reply",
            required: false,
          },
          {
            name: "respond_attach",
            type: ApplicationCommandOptionType.Attachment,
            description: "The attachment to reply",
            required: false,
          },
          {
            name: "respond_sticker",
            type: ApplicationCommandOptionType.String,
            description:
              "The sticker id of target sticker to reply (can only use sticker in this server)",
            required: false,
          },
        ],
      },
      {
        name: "remove",
        type: ApplicationCommandOptionType.Subcommand,
        description: "remove a filter",
        options: [
          {
            name: "filter_id",
            type: ApplicationCommandOptionType.Integer,
            description: "The filter id to remove",
            required: true,
          },
        ],
      },
      {
        name: "list",
        type: ApplicationCommandOptionType.Subcommand,
        description: "List current filter",
        options: [],
      },
    ],
    trans: "filter",
  },
  async execute(client, interaction, args, secret, _, langc) {
    let __;
    const guildid = interaction.guild.id;
    // try {
    const user = interaction.member;
    const status = interaction.options.getSubcommand();
    const filter_type = interaction.options.getString("filter_type");
    const filter = interaction.options.getString("filter");
    const channel_id = interaction.options.getChannel("channel_id");
    const user_id = interaction.options.getUser("user_id");
    const role_id = interaction.options.getRole("role_id");
    const respond_type = interaction.options.getString("respond_type");
    const respond_text = interaction.options.getString("respond_text");
    const respond_attach = await interaction.options.getAttachment(
      "respond_attach"
    );
    const respond_sticker = interaction.options.getString("respond_sticker");
    const filter_id = interaction.options.getInteger("filter_id");

    // Get translation
    const cmd = module.exports.data.name;
    const {
      missing_permission,
      bot,
      mod_yourself,
      mod_log,
      no_perm,
      no_text,
      no_filter,
      invalid_sticker,
      file_size,
      max_file_size,
      filter_added,
      filterid,
      check_with,
      no_configured_filter,
      invalid_id,
      removed_id,
      no_more_filter,
      list_reset,
      eq_to,
      includes,
      sticker_id,
      attch,
      set_by_t,
      channel_filtered,
      user_filtered,
      role_filtered,
      filter_type_t,
      filter_t,
      respond_type_t,
      respond_text_t,
      respond_attach_t,
      respond_sticker_t,
      filter_list,
    } = await trans(cmd, langc, [
      "missing_permission",
      "bot",
      "mod_yourself",
      "mod_log",
      "no_perm",
      "no_text",
      "no_filter",
      "invalid_sticker",
      "file_size",
      "max_file_size",
      "filter_added",
      "filterid",
      "check_with",
      "no_configured_filter",
      "invalid_id",
      "removed_id",
      "no_more_filter",
      "list_reset",
      "eq_to",
      "includes",
      "sticker_id",
      "attch",
      "set_by_t",
      "channel_filtered",
      "user_filtered",
      "role_filtered",
      "filter_type_t",
      "filter_t",
      "respond_type_t",
      "respond_text_t",
      "respond_attach_t",
      "respond_sticker_t",
      "filter_list",
    ]);

    // Permissions check
    if (
      !user.permissions.has(PermissionsBitField.Flags.ModerateMembers) &&
      user.id != secret.me
    ) {
      interaction.reply(`${missing_permission} <a:isis:963826754328330300>`);
      return;
    }

    if (interaction.user.bot) {
      interaction.reply(`${bot} <:pekora_whatwrongwithyou:976146270743855217>`);
      return;
    }

    // Check if the bot has the required permission
    if (
      !interaction.guild.members.me.permissions.has(
        PermissionsBitField.Flags.ManageMessages
      )
    ) {
      interaction.reply(no_perm);
      return;
    }

    if (status === "add") {
      if (!respond_text && !respond_attach && !respond_sticker)
        return interaction.reply(no_text);

      if (filter_type != "attach" && !filter)
        return interaction.reply(no_filter);
      let ch_type;
      if (channel_id) ch_type = channel_id.type === 4 ? "category" : "channel";
      const sticker = respond_sticker
        ? interaction.guild.stickers?.cache.filter(
            (s) => s.id === respond_sticker
          )
        : null;
      if (respond_sticker && sticker.size === 0)
        return interaction.reply(invalid_sticker);

      if (user_id === secret.botid) {
        return interaction.reply(
          `${mod_yourself} <a:z_sui_eating:976448366781267998>`
        );
      }

      if (ch_type === "channel" && channel_id.name.includes("log")) {
        return interaction.reply(`${mod_log} <:emoji_83:1138440779916914698>`);
      } else if (ch_type === "category") {
        const include = channel_id.children.cache.map((channel) =>
          channel.name.includes("log")
        );
        if (include.includes(true))
          return interaction.reply(
            `${mod_log} <:emoji_83:1138440779916914698>`
          );
      }
      const sizelimit = (guild) => {
        switch (guild.premiumTier) {
          case 3:
            return 104857600;
          case 2:
            return 52428800;
          default:
            return 26214400;
        }
      };
      if (respond_attach && respond_attach.size > sizelimit(interaction.guild))
        return interaction.reply(
          `${file_size}\n${max_file_size} ${
            sizelimit(interaction.guild) / 1024 / 1024
          } Mb`
        );

      const filter_group = await db.get("filter_group");
      if (!JSON.stringify(filter_group).includes(guildid))
        await db.push("filter_group", guildid);
      const cache = await db.get(`${guildid}_cache`);
      let id;
      if (!cache) {
        await db.set(`${guildid}_cache`, 1);
        id = 1;
      } else {
        await db.add(`${guildid}_cache`, 1);
        id = cache + 1;
      }

      let filepath;
      if (respond_attach) {
        const response = await fetch(respond_attach.url);
        const path = `filter/${guildid}/${id}`;
        if (!fs.existsSync(path)) {
          fs.mkdirSync(path, { recursive: true });
        }
        filepath = `${path}/${respond_attach.name}`;
        const fileStream = fs.createWriteStream(filepath);
        response.body.pipe(fileStream);

        fileStream.on("finish", () => {
          fileStream.close();
        });
      }

      await db.push(`${guildid}_filter`, id);
      await db.set(`${guildid}_filter_${id}_by`, user.id);
      await db.set(`${guildid}_filter_${id}_type`, filter_type);
      await db.set(`${guildid}_filter_${id}_respond_type`, respond_type);
      if (filter_type === "text" || filter_type === "i_text")
        await db.set(`${guildid}_filter_${id}_filter_text`, filter);
      else if (filter_type === "sticker")
        await db.set(`${guildid}_filter_${id}_filter_stickerid`, filter);
      if (channel_id)
        await db.set(`${guildid}_filter_${id}_filter_channelid`, channel_id.id);
      if (user_id)
        await db.set(`${guildid}_filter_${id}_filter_userid`, user_id.id);
      if (role_id)
        await db.set(`${guildid}_filter_${id}_filter_roleid`, role_id.id);
      if (respond_text)
        await db.set(`${guildid}_filter_${id}_respond_text`, respond_text);
      if (respond_attach)
        await db.set(`${guildid}_filter_${id}_respond_attach`, filepath);
      if (respond_sticker)
        await db.set(
          `${guildid}_filter_${id}_respond_sticker`,
          respond_sticker
        );
      interaction.reply(
        `${filter_added} ${filterid}: ${id}\n${check_with} \`\`/filter list\`\``
      );
    } else if (status === "remove") {
      const filter = await db.get(`${guildid}_filter`);
      if (!filter) return interaction.reply(no_configured_filter);
      if (!filter.includes(filter_id))
        return interaction.reply(
          `${invalid_id} ${check_with} \`\`/filter list\`\``
        );

      await db.pull(`${guildid}_filter`, filter_id);
      await db.delete(`${guildid}_filter_${filter_id}_by`);
      await db.delete(`${guildid}_filter_${filter_id}_type`);
      await db.delete(`${guildid}_filter_${filter_id}_respond_type`);
      await db.delete(`${guildid}_filter_${filter_id}_filter_text`);
      await db.delete(`${guildid}_filter_${filter_id}_filter_stickerid`);
      await db.delete(`${guildid}_filter_${filter_id}_filter_channelid`);
      await db.delete(`${guildid}_filter_${filter_id}_filter_userid`);
      await db.delete(`${guildid}_filter_${filter_id}_filter_roleid`);
      await db.delete(`${guildid}_filter_${filter_id}_respond_text`);
      await db.delete(`${guildid}_filter_${filter_id}_respond_attach`);
      await db.delete(`${guildid}_filter_${filter_id}_respond_sticker`);
      let str = `${removed_id} ${filter_id}.`;

      const filter_length = await db.get(`${guildid}_filter`);
      if (filter_length.length == 0) {
        await db.delete(`${guildid}_filter`);
        await db.pull("filter_group", guildid);
        str += "\n";
        str += no_more_filter;
        await db.delete(`${guildid}_cache`);
        str += "\n";
        str += list_reset;

        const filter_group = await db.get("filter_group");
        if (filter_group.length == 0) await db.delete("filter_group");
      }
      interaction.reply(str);
    } else if (status === "list") {
      const filter = await db.get(`${guildid}_filter`);
      if (!filter) return interaction.reply(no_configured_filter);

      let list = [];
      let list2 = [];
      let list3 = [];

      function gentable(data) {
        if (!Array.isArray(data) || data.length === 0) {
          return "";
        }

        // Get the headers
        const headers = Object.keys(data[0]);

        // Calculate max length of each column
        const colWidths = headers.map((header) => {
          // Ignore for discord user/channel/role id
          const isdc =
            header === set_by_t ||
            header === channel_filtered ||
            header === user_filtered ||
            header === role_filtered;
          return isdc
            ? header.length
            : Math.max(
                header.length,
                ...data.map((row) =>
                  row[header] ? row[header].toString().length : 0
                )
              );
        });

        // Pad each cell to the column width
        const padCell = (cell, index) => {
          return cell.toString().padEnd(colWidths[index], " ");
        };

        // Generate header row
        const headerRow = headers.map(padCell).join(" | ");
        const divider = colWidths.map((width) => "-".repeat(width)).join("-|-");
        const tableRows = data.map((row) => {
          return headers
            .map((header) =>
              padCell(row[header] || "", headers.indexOf(header))
            )
            .join(" | ");
        });

        // Combine all rows into a single string
        const tableString = [headerRow, divider, ...tableRows].join("\n");
        return tableString;
      }

      for (const id of filter) {
        const set_by = await db.get(`${guildid}_filter_${id}_by`);
        const filter_type = await db.get(`${guildid}_filter_${id}_type`);
        let filter;
        if (filter_type === "text" || filter_type === "i_text")
          filter = await db.get(`${guildid}_filter_${id}_filter_text`);
        else if (filter_type === "sticker")
          filter = await db.get(`${guildid}_filter_${id}_filter_stickerid`);
        const channel_id = await db.get(
          `${guildid}_filter_${id}_filter_channelid`
        );
        const user_id = await db.get(`${guildid}_filter_${id}_filter_userid`);
        const role_id = await db.get(`${guildid}_filter_${id}_filter_roleid`);
        const respond_type = await db.get(
          `${guildid}_filter_${id}_respond_type`
        );
        const respond_text = await db.get(
          `${guildid}_filter_${id}_respond_text`
        );
        const respond_attach = await db.get(
          `${guildid}_filter_${id}_respond_attach`
        );
        const respond_sticker_id = await db.get(
          `${guildid}_filter_${id}_respond_sticker`
        );

        let filter_type_text;
        if (filter_type === "text") filter_type_text = eq_to;
        else if (filter_type === "i_text") filter_type_text = includes;
        else if (filter_type === "sticker") filter_type_text = sticker_id;
        else if (filter_type === "attach") filter_type_text = attch;

        const table = {
          "ID": id,
          [set_by_t]: `<@${set_by}>`,
          [channel_filtered]: channel_id ? `<#${channel_id}>` : "null",
          [user_filtered]: user_id ? `<@${user_id}>` : "null",
          [role_filtered]: role_id ? `<@${role_id}>` : "null",
        };
        const table2 = {
          "ID": id,
          [filter_type_t]: filter_type_text,
          [filter_t]: filter ? filter : "null",
          [respond_type_t]: respond_type,
        };
        const table3 = {
          "ID": id,
          [respond_text_t]: respond_text ? respond_text : "null",
          [respond_attach_t]: respond_attach
            ? respond_attach.split(`/${id}/`)[1]
            : "null",
          [respond_sticker_t]: respond_sticker_id ? respond_sticker_id : "null",
        };

        list.push(table);
        list2.push(table2);
        list3.push(table3);
      }

      interaction.reply(filter_list);
      split(gentable(list), interaction.channel, __, __, true);
      split(
        `\`\`\`${gentable(list2)}\`\`\``,
        interaction.channel,
        __,
        __,
        true
      );
      split(
        `\`\`\`${gentable(list3)}\`\`\``,
        interaction.channel,
        __,
        __,
        true
      );
    }
  },
};
