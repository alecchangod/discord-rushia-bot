const {
  ActionRowBuilder,
  PermissionsBitField,
  ApplicationCommandOptionType,
  ButtonBuilder,
  ButtonStyle,
  EmbedBuilder,
} = require("discord.js");
const fs = require("fs");
const https = require("https");
const wait = require("node:timers/promises").setTimeout;
const trans = require("../../function/common/trans.js");
const fetch = require("node-fetch");
const { QuickDB } = require("quick.db");
const bot = new QuickDB({ filePath: "database/bot.sqlite" });
const db = new QuickDB({ filePath: "database/profile.sqlite" });

module.exports = {
  data: {
    name: "profile",
    description: "Create profile for webhook, say, and filter",
    options: [
      {
        name: "create",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Create a new profile",
        options: [
          {
            name: "name",
            type: ApplicationCommandOptionType.String,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "avatar_link",
            type: ApplicationCommandOptionType.String,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "avatar_attach",
            type: ApplicationCommandOptionType.Attachment,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "copy_from",
            type: ApplicationCommandOptionType.User,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "auto_update",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "set_default",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "default_all_server",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
        ],
      },
      {
        name: "default",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Enable default profile",
        options: [
          {
            name: "profile_id",
            type: ApplicationCommandOptionType.Integer,
            description: "The filter to add (ignore this for attachments)",
            required: true,
          },
          {
            name: "default_all_server",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
        ],
      },
      {
        name: "delete",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Send message normally",
        options: [
          {
            name: "profile_id",
            type: ApplicationCommandOptionType.Integer,
            description: "The filter to add (ignore this for attachments)",
            required: true,
          },
        ],
      },
      {
        name: "deleteall",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Send message normally",
        options: [],
      },
      {
        name: "edit",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Edit current profile",
        options: [
          {
            name: "profile_id",
            type: ApplicationCommandOptionType.Integer,
            description: "The filter to add (ignore this for attachments)",
            required: true,
          },
          {
            name: "name",
            type: ApplicationCommandOptionType.String,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "avatar_link",
            type: ApplicationCommandOptionType.String,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "avatar_attach",
            type: ApplicationCommandOptionType.Attachment,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "remove_avatar",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "copy_from",
            type: ApplicationCommandOptionType.User,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "auto_update",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "set_default",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
          {
            name: "default_all_server",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
        ],
      },
      {
        name: "list",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Edit current profile",
        options: [],
      },
      {
        name: "unsetdefault",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Disahle default profile",
        options: [
          {
            name: "default_all_server",
            type: ApplicationCommandOptionType.Boolean,
            description: "The filter to add (ignore this for attachments)",
            required: false,
          },
        ],
      },
      {
        name: "update",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Update profile",
        options: [
          {
            name: "profile_id",
            type: ApplicationCommandOptionType.Integer,
            description: "The filter to add (ignore this for attachments)",
            required: true,
          },
        ],
      },
    ],
    trans: "profile",
  },
  async execute(client, interaction, args, secret, _, langc, guild) {
    try {
      const status = interaction.options.getSubcommand();
      const profile_name = interaction.options.getString("name");
      const profile_avatar_link = interaction.options.getString("avatar_link");
      const profile_avatar_attach =
        interaction.options.getAttachment("avatar_attach");
      const profile_copy_from = interaction.options.getUser("copy_from");
      const profile_auto_update = interaction.options.getBoolean("auto_update");
      const profile_set_default = interaction.options.getBoolean("set_default");
      const profile_default_all_server =
        interaction.options.getBoolean("default_all_server");
      const modify_profile_id = interaction.options.getInteger("profile_id");
      const profile_remove_avatar =
        interaction.options.getBoolean("remove_avatar");
      let avatar_url, avatar_status, uname;
      let str = "";

      if (status === "create") {
        const cache = await db.get(`${interaction.member.id}_cache`);
        let id;
        if (!cache) {
          id = 1;
        } else {
          id = cache + 1;
        }
        if (!profile_copy_from) {
          if (profile_auto_update)
            str +=
              "\nAuto update will be ignored as it was only available for copy_from.";
          if (!profile_name)
            return interaction.reply({
              content: "Must have name provided for profile.",
              ephemeral: true,
            });
          uname = profile_name;
          if (!profile_avatar_link && !profile_avatar_attach)
            str += "\nNo avatar will be used for new profile.";
          else {
            if (profile_avatar_link && profile_avatar_attach)
              str +=
                "\nBoth avatar link and attachments was provided. Only the avatar attachments will be used.";
            if (profile_avatar_attach)
              avatar_url = profile_avatar_attach.attachment;
            else if (profile_avatar_link) avatar_url = profile_avatar_link;
          }
          str += "\nProfile successfully created.";
        }
        if (profile_copy_from) {
          str +=
            profile_avatar_link || profile_avatar_attach || profile_name
              ? "\nCopy from user was choosen."
              : "";
          if (profile_avatar_link || profile_avatar_attach)
            str += "\nAvatar will be ignored.";
          if (profile_name) str += "\nName will be ignored.";
          await db.set(`${interaction.member.id}_profile_${id}_copy_from`, {
            guild: interaction.guild.id,
            user: profile_copy_from.id,
          });
          if (profile_auto_update === true)
            await db.set(
              `${interaction.member.id}_profile_${id}_autoupdate`,
              profile_auto_update
            );
          const user = await interaction.guild.members.fetch(
            profile_copy_from.id
          );
          uname = user.nickname
            ? user.nickname
            : profile_copy_from.globalName
            ? profile_copy_from.globalName
            : user.user.username;

          str += "\nProfile successfully copied.";
        }
        await interaction.deferReply({ ephemeral: true });
        async function fetchAvatarUrl(profile_copy_from, interaction) {
          return new Promise((resolve, reject) => {
            let options = {
              method: "GET",
              headers: {
                Authorization: `Bot ${secret.token}`,
              },
              hostname: "discord.com",
              path: `/api/guilds/${interaction.guild.id}/members/${profile_copy_from.id}`,
            };

            const req = https.request(options, (res) => {
              res.setEncoding("utf8");
              let val = "";
              res.on("data", (chunk) => {
                val += chunk;
              });
              res.on("end", () => {
                try {
                  const data = JSON.parse(val);
                  let avatar_ext;
                  if (data.avatar)
                    avatar_ext = data.avatar.startsWith("a_") ? "gif" : "webp";
                  let url = `https://cdn.discordapp.com/guilds/${interaction.guild.id}/users/${profile_copy_from.id}/avatars/${data.avatar}.${avatar_ext}`;
                  let avatar_url = data.avatar
                    ? url
                    : profile_copy_from.displayAvatarURL();
                  resolve(avatar_url);
                } catch (error) {
                  reject(error);
                }
              });
            });

            req.on("error", (e) => {
              reject(e);
            });

            req.end();
          });
        }
        if (profile_copy_from)
          avatar_url = await fetchAvatarUrl(profile_copy_from, interaction);

        str += `\nProfile ID: \`\`${id}\`\``;
        if (avatar_url)
          avatar_status = await fetchavatar(
            interaction,
            id,
            avatar_url,
            secret,
            db
          );

        // Give the bot 15 second incase the token expired
        if (avatar_url && !avatar_status) await wait(15000);
        if (!avatar_url || avatar_status === true) {
          if (!cache) {
            await db.set(`${interaction.member.id}_cache`, 1);
          } else {
            await db.add(`${interaction.member.id}_cache`, 1);
          }
          await db.push(`${interaction.member.id}_profile`, id);
          if (profile_default_all_server === true) {
            await db.set(`${interaction.member.id}_default`, id);
          }
          if (profile_set_default === true) {
            await db.set(
              `${interaction.member.id}_${interaction.guild.id}_default`,
              id
            );
          }
          str += profile_default_all_server
            ? "\nProfile have been set as default for all server."
            : profile_set_default
            ? "\nProfile have been set as default in this server."
            : "";
          str +=
            profile_default_all_server || profile_set_default
              ? "\nI will use this profile as default for ``/webhook`` message."
              : "";

          await db.set(`${interaction.member.id}_profile_${id}_name`, uname);
          str += `\nProfile Name: ${uname}`;
          interaction.editReply({ content: str, ephemeral: true });
        }
      } else if (status === "default") {
        await db.set(
          `${interaction.member.id}_${interaction.guild.id}_default`,
          modify_profile_id
        );
        if (profile_default_all_server === true) {
          await db.set(`${interaction.member.id}_default`, modify_profile_id);
        }

        str += profile_default_all_server
          ? "\nProfile have been set as default for all server."
          : "\nProfile have been set as default in this server.";
        str += "\nI will use this profile as default for ``/webhook`` message.";
        const name = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_name`
        );
        str += `\nProfile ID: ${modify_profile_id}`;
        str += `\nProfile Name: ${name}`;

        interaction.reply({ content: str, ephemeral: true });
      } else if (status === "delete") {
        const profile_current = await db.get(
          `${interaction.member.id}_profile`
        );
        if (!profile_current.includes(modify_profile_id))
          return interaction.reply({
            content:
              "Profile doesn't exist. It might be deleted or not yet created.",
            ephemeral: true,
          });
        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_name`
        );
        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_copy_from`
        );
        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_autoupdate`
        );

        const performRequest = (options, data = null) => {
          return new Promise((resolve, reject) => {
            const req = https.request(options, (res) => {
              let response = "";
              res.on("data", (chunk) => {
                response += chunk;
              });
              res.on("end", () => {
                resolve(response);
              });
            });
            req.on("error", reject);
            if (data) {
              req.write(data);
            }
            req.end();
          });
        };

        const dropbox_token = await bot.get("dropbox_token");
        const avatar_filepath = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_avatar_path`
        );
        let optionsdata = JSON.stringify({
          path: `/bot/${avatar_filepath}`,
        });
        let options = {
          method: "POST",
          headers: {
            Authorization: `Bearer ${dropbox_token}`,
            "Content-Type": "application/json",
          },
        };

        options.hostname = "api.dropboxapi.com";
        options.path = "/2/files/delete_v2";

        try {
          const linkResponse = await performRequest(options, optionsdata);
          const jsonResponse = JSON.parse(linkResponse);
          if (
            jsonResponse.error_summary === "expired_access_token/" ||
            jsonResponse.error_summary === "invalid_access_token/"
          ) {
            const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

            const token_options = {
              hostname: "api.dropbox.com",
              path: "/oauth2/token",
              method: "POST",
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization:
                  "Basic " +
                  Buffer.from(
                    secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                  ).toString("base64"),
                "Content-Length": Buffer.byteLength(postData),
              },
            };

            const linkResponse = await performRequest(token_options, postData);
            const jsonResponse = JSON.parse(linkResponse);
            const dropbox_token = await bot.set(
              "dropbox_token",
              jsonResponse.access_token
            );

            // Delete the file again
            let options = {
              method: "POST",
              headers: {
                Authorization: `Bearer ${dropbox_token}`,
                "Content-Type": "application/json",
              },
            };

            options.hostname = "api.dropboxapi.com";
            options.path = "/2/files/delete_v2";

            try {
              await performRequest(options, optionsdata);
            } catch (error) {
              console.error("Error deleting file:", error);
              return;
            }
          }
        } catch (error) {
          console.error("Error deleting file:", error);
          return;
        }

        fs.rmSync(avatar_filepath, { recursive: true, force: true });

        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_avatar`
        );
        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_avatar_path`
        );
        await db.delete(
          `${interaction.member.id}_profile_${modify_profile_id}_avatar_filename`
        );

        await db.pull(`${interaction.member.id}_profile`, modify_profile_id);
        const default_id = await db.get(`${interaction.member.id}_default`);
        const server_default_id = await db.get(
          `${interaction.member.id}_${interaction.guild.id}_default`
        );
        if (default_id === modify_profile_id)
          await db.delete(`${interaction.member.id}_default`);
        if (server_default_id === modify_profile_id)
          await db.delete(
            `${interaction.member.id}_${interaction.guild.id}_default`
          );
        str += `Successfully deleted profile \`\`${modify_profile_id}\`\``;
        interaction.reply({ content: str, ephemeral: true });
      } else if (status === "deleteall") {
        let delete_items = await db.all();
        delete_items = delete_items
          .map((value) => value.id)
          .filter((id) => id.startsWith(`${interaction.member.id}`));
        if (!delete_items.length)
          return interaction.reply({
            content: "You haven't created any profile yet!",
            ephemeral: true,
          });

        await interaction.deferReply({ ephemeral: true });

        const Embed = new EmbedBuilder()
          .setColor(0xff0000)
          .setTitle("Are you sure to delete all profiles created?")
          .setDescription("You won't be able to undo this!");

        const confirm = new ButtonBuilder()
          .setCustomId("profile_deleteall_confirm")
          .setLabel("Confirm")
          .setStyle(ButtonStyle.Danger);

        const cancel = new ButtonBuilder()
          .setCustomId("profile_deleteall_cancel")
          .setLabel("Cancel")
          .setStyle(ButtonStyle.Secondary);

        const row = new ActionRowBuilder().addComponents(cancel, confirm);

        await interaction.editReply({
          embeds: [Embed],
          components: [row],
        });

        const collector = interaction.channel.createMessageComponentCollector({
          time: 60000,
        });

        collector.on("collect", async (i) => {
          if (i.member.id != interaction.user.id) {
            return i.reply({
              content: `This interaction is not for you`,
              ephemeral: true,
            });
          }

          if (i.customId == "profile_deleteall_confirm") {
            delete_items.forEach(async (key) => {
              await db.delete(key);
            });
            await wait(1000);
            str += `Successfully deleted all profiles.`;

            const performRequest = (options, data = null) => {
              return new Promise((resolve, reject) => {
                const req = https.request(options, (res) => {
                  let response = "";
                  res.on("data", (chunk) => {
                    response += chunk;
                  });
                  res.on("end", () => {
                    resolve(response);
                  });
                });
                req.on("error", reject);
                if (data) {
                  req.write(data);
                }
                req.end();
              });
            };

            const dropbox_token = await bot.get("dropbox_token");
            const avatar_filepath = `avatar/${interaction.member.id}`;
            let optionsdata = JSON.stringify({
              path: `/bot/${avatar_filepath}`,
            });
            let options = {
              method: "POST",
              headers: {
                Authorization: `Bearer ${dropbox_token}`,
                "Content-Type": "application/json",
              },
            };

            options.hostname = "api.dropboxapi.com";
            options.path = "/2/files/delete_v2";

            try {
              const linkResponse = await performRequest(options, optionsdata);
              const jsonResponse = JSON.parse(linkResponse);
              if (
                jsonResponse.error_summary === "expired_access_token/" ||
                jsonResponse.error_summary === "invalid_access_token/"
              ) {
                const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

                const token_options = {
                  hostname: "api.dropbox.com",
                  path: "/oauth2/token",
                  method: "POST",
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    Authorization:
                      "Basic " +
                      Buffer.from(
                        secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                      ).toString("base64"),
                    "Content-Length": Buffer.byteLength(postData),
                  },
                };

                const linkResponse = await performRequest(
                  token_options,
                  postData
                );
                const jsonResponse = JSON.parse(linkResponse);
                const dropbox_token = await bot.set(
                  "dropbox_token",
                  jsonResponse.access_token
                );

                // Delete the file again
                let options = {
                  method: "POST",
                  headers: {
                    Authorization: `Bearer ${dropbox_token}`,
                    "Content-Type": "application/json",
                  },
                };

                options.hostname = "api.dropboxapi.com";
                options.path = "/2/files/delete_v2";

                try {
                  await performRequest(options, optionsdata);
                } catch (error) {
                  console.error("Error deleting file:", error);
                  return;
                }
              }
            } catch (error) {
              console.error("Error deleting file:", error);
              return;
            }

            fs.rmSync(avatar_filepath, { recursive: true, force: true });

            interaction.editReply({
              content: str,
              embeds: [],
              components: [],
              ephemeral: true,
            });
            collector.stop();
          }

          if (i.customId == "profile_deleteall_cancel") {
            await interaction.editReply({
              content: "This operation has been canceled.",
              embeds: [],
              components: [],
              ephemeral: true,
            });
            collector.stop();
          }
        });

        collector.on("end", (collected) => {
          if (collected.size === 0)
            interaction.editReply({
              content: "Operation canceled due to timeouted.",
              embeds: [],
              components: [],
            });
        });
      } else if (status === "edit") {
        const profile_current = await db.get(
          `${interaction.member.id}_profile`
        );
        if (!profile_current.includes(modify_profile_id))
          return interaction.reply({
            content:
              "Profile doesn't exist. It might be deleted or not yet created.",
            ephemeral: true,
          });
        const was_copy_from = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_copy_from`
        );
        const was_auto_update = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_autoupdate`
        );

        await interaction.deferReply({ ephemeral: true });
        if (!was_copy_from && profile_auto_update === true) {
          str +=
            "\nAuto update will be ignored as it was only available for copy_from.";
        }
        uname = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_name`
        );
        if (
          profile_auto_update === true ||
          (was_auto_update === true && profile_auto_update === true)
        )
          str +=
            "\nProfile name will be ignored as this profile will be auto updated.";
        else if (profile_name) uname = profile_name;
        if (profile_remove_avatar) {
          if (
            profile_copy_from &&
            (profile_auto_update === true ||
              (was_auto_update === true && profile_auto_update === true))
          )
            str +=
              "\nRemove avatar will be ignored as auto update was enabled.";
          const current_avatar = await db.get(
            `${interaction.member.id}_profile_${modify_profile_id}_avatar`
          );
          if (!current_avatar) return;

          const performRequest = (options, data = null) => {
            return new Promise((resolve, reject) => {
              const req = https.request(options, (res) => {
                let response = "";
                res.on("data", (chunk) => {
                  response += chunk;
                });
                res.on("end", () => {
                  resolve(response);
                });
              });
              req.on("error", reject);
              if (data) {
                req.write(data);
              }
              req.end();
            });
          };

          const dropbox_token = await bot.get("dropbox_token");
          const avatar_filepath = await db.get(
            `${interaction.member.id}_profile_${modify_profile_id}_avatar_path`
          );
          let optionsdata = JSON.stringify({
            path: `/bot/${avatar_filepath}`,
          });
          let options = {
            method: "POST",
            headers: {
              Authorization: `Bearer ${dropbox_token}`,
              "Content-Type": "application/json",
            },
          };

          options.hostname = "api.dropboxapi.com";
          options.path = "/2/files/delete_v2";

          try {
            const linkResponse = await performRequest(options, optionsdata);
            const jsonResponse = JSON.parse(linkResponse);
            if (
              jsonResponse.error_summary === "expired_access_token/" ||
              jsonResponse.error_summary === "invalid_access_token/"
            ) {
              const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

              const token_options = {
                hostname: "api.dropbox.com",
                path: "/oauth2/token",
                method: "POST",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  Authorization:
                    "Basic " +
                    Buffer.from(
                      secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                    ).toString("base64"),
                  "Content-Length": Buffer.byteLength(postData),
                },
              };

              const linkResponse = await performRequest(
                token_options,
                postData
              );
              const jsonResponse = JSON.parse(linkResponse);
              const dropbox_token = await bot.set(
                "dropbox_token",
                jsonResponse.access_token
              );

              // Delete the file again
              let options = {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${dropbox_token}`,
                  "Content-Type": "application/json",
                },
              };

              options.hostname = "api.dropboxapi.com";
              options.path = "/2/files/delete_v2";

              try {
                await performRequest(options, optionsdata);
              } catch (error) {
                console.error("Error deleting file:", error);
                return;
              }
            }
          } catch (error) {
            console.error("Error deleting file:", error);
            return;
          }

          fs.rmSync(avatar_filepath, { recursive: true, force: true });

          await db.delete(
            `${interaction.member.id}_profile_${modify_profile_id}_avatar`
          );
          await db.delete(
            `${interaction.member.id}_profile_${modify_profile_id}_avatar_path`
          );
          await db.delete(
            `${interaction.member.id}_profile_${modify_profile_id}_avatar_filename`
          );

          str += "\nNo avatar will be used for new profile.";
        } else {
          if (profile_avatar_link && profile_avatar_attach)
            str +=
              "\nBoth avatar link and attachments was provided. Only the avatar attachments will be used.";
          if (profile_avatar_attach)
            avatar_url = profile_avatar_attach.attachment;
          else if (profile_avatar_link) avatar_url = profile_avatar_link;
        }
        if (
          profile_remove_avatar &&
          (profile_avatar_link || profile_avatar_attach)
        )
          return interaction.editReply(
            "Remove avatar can't be used when avatar link/attachment was provided!"
          );
        if (profile_copy_from) {
          str +=
            profile_avatar_link || profile_avatar_attach || profile_name
              ? "\nCopy from user was choosen."
              : "";
          if (
            profile_avatar_link ||
            profile_avatar_attach ||
            profile_remove_avatar
          )
            str += `\nAvatar ${
              profile_avatar_link || profile_avatar_attach ? "file " : ""
            }will be ignored.`;
          if (profile_name) str += "\nName will be ignored.";
          await db.set(
            `${interaction.member.id}_profile_${modify_profile_id}_copy_from`,
            {
              guild: interaction.guild.id,
              user: profile_copy_from.id,
            }
          );
          if (profile_auto_update === true) {
            await db.set(
              `${interaction.member.id}_profile_${modify_profile_id}_autoupdate`,
              profile_auto_update
            );
            str += "\nAuto update for this profile have been enabled.";
          } else if (profile_auto_update === false) {
            await db.set(
              `${interaction.member.id}_profile_${modify_profile_id}_autoupdate`,
              profile_auto_update
            );
            str += "\nAuto update for this profile have been disabled.";
          }
          const user = await interaction.guild.members.fetch(
            profile_copy_from.id
          );
          uname = user.nickname
            ? user.nickname
            : profile_copy_from.globalName
            ? profile_copy_from.globalName
            : user.user.username;
        }
        async function fetchAvatarUrl(profile_copy_from, interaction) {
          return new Promise((resolve, reject) => {
            let options = {
              method: "GET",
              headers: {
                Authorization: `Bot ${secret.token}`,
              },
              hostname: "discord.com",
              path: `/api/guilds/${interaction.guild.id}/members/${profile_copy_from.id}`,
            };

            const req = https.request(options, (res) => {
              res.setEncoding("utf8");
              let val = "";
              res.on("data", (chunk) => {
                val += chunk;
              });
              res.on("end", () => {
                try {
                  const data = JSON.parse(val);
                  let avatar_ext;
                  if (data.avatar)
                    avatar_ext = data.avatar.startsWith("a_") ? "gif" : "webp";
                  let url = `https://cdn.discordapp.com/guilds/${interaction.guild.id}/users/${profile_copy_from.id}/avatars/${data.avatar}.${avatar_ext}`;
                  let avatar_url = data.avatar
                    ? url
                    : profile_copy_from.displayAvatarURL();
                  resolve(avatar_url);
                } catch (error) {
                  reject(error);
                }
              });
            });

            req.on("error", (e) => {
              reject(e);
            });

            req.end();
          });
        }
        if (profile_copy_from && profile_remove_avatar !== true)
          avatar_url = await fetchAvatarUrl(profile_copy_from, interaction);

        if (avatar_url)
          avatar_status = await fetchavatar(
            interaction,
            id,
            avatar_url,
            secret,
            db
          );

        // Give the bot 15 second incase the token expired
        if (avatar_url && !avatar_status) await wait(15000);
        if (!avatar_url || avatar_status === true) {
          if (profile_default_all_server === true) {
            await db.set(`${interaction.member.id}_default`, id);
          }
          if (profile_set_default === true) {
            await db.set(
              `${interaction.member.id}_${interaction.guild.id}_default`,
              id
            );
          }
          str += profile_default_all_server
            ? "\nProfile have been set as default for all server."
            : profile_set_default
            ? "\nProfile have been set as default in this server."
            : "";

          str +=
            profile_default_all_server || profile_set_default
              ? "\nI will use this profile as default for ``/webhook`` message."
              : "";

          if (!profile_auto_update)
            await db.set(
              `${interaction.member.id}_profile_${modify_profile_id}_name`,
              uname
            );
          str += `\nProfile ID: \`\`${modify_profile_id}\`\``;
          str += `\nProfile Name: \`\`${uname}\`\``;
          if (
            !profile_name &&
            !profile_avatar_link &&
            !profile_avatar_attach &&
            !profile_avatar_attach &&
            !profile_remove_avatar &&
            !profile_copy_from &&
            !profile_auto_update &&
            !profile_set_default &&
            !profile_default_all_server
          )
            str = "Please provide something to edit!";
          interaction.editReply({ content: str, ephemeral: true });
        }
      } else if (status === "list") {
        const current_profile = await db.get(
          `${interaction.member.id}_profile`
        );
        let maxpage = Math.ceil(current_profile.length / 3);
        let currentpage = 1;

        let row = new ActionRowBuilder()
          .addComponents(
            (firstBtn = new ButtonBuilder()
              .setCustomId("profile_list_first")
              .setLabel("<<<")
              .setStyle(ButtonStyle.Success)
              .setDisabled(true))
          )
          .addComponents(
            (previousBtn = new ButtonBuilder()
              .setCustomId("profile_list_previous")
              .setLabel("<<")
              .setStyle(ButtonStyle.Primary)
              .setDisabled(true))
          )
          .addComponents(
            (nextBtn = new ButtonBuilder()
              .setCustomId("profile_list_next")
              .setLabel(">>")
              .setStyle(ButtonStyle.Primary))
          )
          .addComponents(
            (lastBtn = new ButtonBuilder()
              .setCustomId("profile_list_last")
              .setLabel(">>>")
              .setStyle(ButtonStyle.Success))
          );

        let list_name = [],
          list_copy_from = [],
          list_auto_update = [],
          list_was_default = [],
          list_profileid = [];
        let list = {};
        let userid = interaction.member.id;

        for (let profile = 0; profile < current_profile.length; profile++) {
          let id = (list_profileid[profile] = current_profile[profile]);
          let page = Math.ceil((profile + 1) / 3);
          list_name[id] = await db.get(`${userid}_profile_${id}_name`);
          let was_copy_from = await db.get(`${userid}_profile_${id}_copy_from`);
          list_copy_from[id] = was_copy_from ? true : false;
          let was_auto_update = await db.get(
            `${userid}_profile_${id}_autoupdate`
          );
          list_auto_update[id] = was_auto_update ? true : false;
          list_was_default[id] = await db.get(`${userid}_profile_${id}`);
          if (!list[page])
            list[page] = new EmbedBuilder()
              .setTitle("List of profiles")
              .setFooter({ text: `Page ${page}/${maxpage}` });
          list[page].addFields({
            name: id.toString(),
            value: `name: \`\`${list_name[id]}\`\` copy_from: \`\`${list_copy_from[id]}\`\` auto_update: \`\`${list_auto_update[id]}\`\` was_default: \`\`${list_was_default[id]}\`\``,
          });
        }

        interaction.reply({
          embeds: [list[currentpage]],
          components: [row],
          ephemeral: true,
        });

        const collector = interaction.channel.createMessageComponentCollector({
          time: 600000,
        });

        collector.on("collect", async (i) => {
          if (i.member.id != interaction.user.id) {
            return i.reply({
              content: `This interaction is not for you`,
              ephemeral: true,
            });
          }

          if (i.customId == "profile_list_first") {
            await i.deferUpdate();

            currentpage = 1;
            firstBtn.setDisabled(true);
            previousBtn.setDisabled(true);

            if (currentpage !== maxpage) {
              nextBtn.setDisabled(false);
              lastBtn.setDisabled(false);
            } else {
              nextBtn.setDisabled(true);
              lastBtn.setDisabled(true);
            }

            interaction.editReply({
              embeds: [list[currentpage]],
              components: [row],
              ephemeral: true,
            });
          } else if (i.customId == "profile_list_previous") {
            await i.deferUpdate();

            currentpage -= 1;
            if (currentpage === 1) {
              firstBtn.setDisabled(true);
              previousBtn.setDisabled(true);
            } else {
              firstBtn.setDisabled(false);
              previousBtn.setDisabled(false);
            }
            if (currentpage !== maxpage) {
              nextBtn.setDisabled(false);
              lastBtn.setDisabled(false);
            } else {
              nextBtn.setDisabled(true);
              lastBtn.setDisabled(true);
            }

            interaction.editReply({
              embeds: [list[currentpage]],
              components: [row],

              ephemeral: true,
            });
          } else if (i.customId == "profile_list_next") {
            await i.deferUpdate();

            currentpage++;
            firstBtn.setDisabled(false);
            previousBtn.setDisabled(false);
            if (currentpage !== maxpage) {
              nextBtn.setDisabled(false);
              lastBtn.setDisabled(false);
            } else {
              nextBtn.setDisabled(true);
              lastBtn.setDisabled(true);
            }

            interaction.editReply({
              embeds: [list[currentpage]],
              components: [row],

              ephemeral: true,
            });
          } else if (i.customId == "profile_list_last") {
            await i.deferUpdate();

            currentpage = maxpage;
            firstBtn.setDisabled(false);
            previousBtn.setDisabled(false);
            if (currentpage !== maxpage) {
              nextBtn.setDisabled(false);
              lastBtn.setDisabled(false);
            } else {
              nextBtn.setDisabled(true);
              lastBtn.setDisabled(true);
            }

            interaction.editReply({
              embeds: [list[currentpage]],
              components: [row],

              ephemeral: true,
            });
          }
        });

        collector.on("end", (collected) => {
          if (collected.size === 0)
            interaction.editReply({
              content: "Operation canceled due to timeouted.",
              embeds: [],
              components: [],
            });
        });
      } else if (status === "unsetdefault") {
        let delete_items = await db.all();
        delete_items = delete_items
          .map((value) => value.id)
          .filter((id) =>
            id.startsWith(
              `${interaction.member.id}${
                profile_default_all_server ? "" : `_${interaction.guild.id}`
              }`
            )
          )
          .filter((id) => id.endsWith(`default`));
        if (!delete_items.length)
          return interaction.reply({
            content: "You haven't defaulted any profile yet!",
            ephemeral: true,
          });

        await interaction.deferReply({ ephemeral: true });

        const Embed = new EmbedBuilder()
          .setColor(0xff0000)
          .setTitle(
            profile_default_all_server
              ? `Are you sure to unset ${delete_items.length} default profile${
                  delete_items.length > 1 ? "s" : ""
                }?`
              : "Are you sure to unset default profile for this server?"
          )
          .setDescription("You won't be able to undo this!");

        const confirm = new ButtonBuilder()
          .setCustomId("profile_unsetdefault_confirm")
          .setLabel("Confirm")
          .setStyle(ButtonStyle.Danger);

        const cancel = new ButtonBuilder()
          .setCustomId("profile_unsetdefault_cancel")
          .setLabel("Cancel")
          .setStyle(ButtonStyle.Secondary);

        const row = new ActionRowBuilder().addComponents(cancel, confirm);

        await interaction.editReply({
          embeds: [Embed],
          components: [row],
        });

        const collector = interaction.channel.createMessageComponentCollector({
          time: 60000,
        });

        collector.on("collect", async (i) => {
          if (i.member.id != interaction.user.id) {
            return i.reply({
              content: `This interaction is not for you`,
              ephemeral: true,
            });
          }

          if (i.customId == "profile_unsetdefault_confirm") {
            delete_items.forEach(async (key) => {
              await db.delete(key);
            });

            str += profile_default_all_server
              ? `Successfully unset ${delete_items.length} profile${
                  delete_items.length > 1 ? "s" : ""
                } for all server.`
              : "Successfully unset default profile for this server.";

            interaction.editReply({
              content: str,
              embeds: [],
              components: [],
              ephemeral: true,
            });
            collector.stop();
          }

          if (i.customId == "profile_unsetdefault_cancel") {
            await interaction.editReply({
              content: "This operation has been canceled.",
              embeds: [],
              components: [],
              ephemeral: true,
            });
            collector.stop();
          }
        });

        collector.on("end", (collected) => {
          if (collected.size === 0)
            interaction.editReply({
              content: "Operation canceled due to timeouted.",
              embeds: [],
              components: [],
            });
        });
      } else if (status === "update") {
        const profile_auto_update = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_autoupdate`
        );
        if (profile_auto_update)
          str +=
            "Auto update was enabled for this profile.\nRunning this command is useless as it will auto update when you use ``/webhook`` to send message.";

        const profile_current = await db.get(
          `${interaction.member.id}_profile`
        );
        if (!profile_current.includes(modify_profile_id))
          return interaction.reply({
            content:
              "Profile doesn't exist. It might be deleted or not yet created.",
            ephemeral: true,
          });

        const profile_copy_from = await db.get(
          `${interaction.member.id}_profile_${modify_profile_id}_copy_from`
        );
        if (!profile_copy_from)
          return interaction.reply({
            content:
              "Update profile won't work if it wasn't created using ``copy_from``",
            ephemeral: true,
          });
        const guild = client.guilds.cache.get(profile_copy_from.guild);
        if (!guild)
          return interaction.reply({
            content: `I am not in the guild ${profile_copy_from.guild}`,
            ephemeral: true,
          });
        const user = await guild.members.fetch(profile_copy_from.user);
        if (!user)
          return interaction.reply({
            content: `<@${profile_copy_from.user}> (${profile_copy_from.user}) was no longer in the guild ${profile_copy_from.guild}`,
            ephemeral: true,
          });
        const profile_name = user.nickname
          ? user.nickname
          : profile_copy_from.globalName
          ? profile_copy_from.globalName
          : user.user.username;

        await db.set(
          `${interaction.member.id}_profile_${modify_profile_id}_name`,
          profile_name
        );

        await interaction.deferReply({ ephemeral: true });

        // Functions for profile update

        async function uploadAndGetLink(
          avatar_filepath,
          buffer,
          secret,
          user,
          id
        ) {
          const performRequest = (options, data = null) => {
            return new Promise((resolve, reject) => {
              const req = https.request(options, (res) => {
                let response = "";
                res.on("data", (chunk) => {
                  response += chunk;
                });
                res.on("end", () => {
                  resolve(response);
                });
              });
              req.on("error", reject);
              if (data) {
                req.write(data);
              }
              req.end();
            });
          };

          const dropbox_token = await bot.get("dropbox_token");

          let del_avatar_filepath = await db.get(
            `${user}_profile_${id}_avatar_path`
          );
          del_avatar_filepath += "/archived";
          let optionsdata = JSON.stringify({
            path: `/bot/${del_avatar_filepath}`,
          });
          let options = {
            method: "POST",
            headers: {
              Authorization: `Bearer ${dropbox_token}`,
              "Content-Type": "application/json",
            },
          };

          options.hostname = "api.dropboxapi.com";
          options.path = "/2/files/delete_v2";

          try {
            const linkResponse = await performRequest(options, optionsdata);
            const jsonResponse = JSON.parse(linkResponse);
            if (
              jsonResponse.error_summary === "expired_access_token/" ||
              jsonResponse.error_summary === "invalid_access_token/"
            ) {
              const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

              const token_options = {
                hostname: "api.dropbox.com",
                path: "/oauth2/token",
                method: "POST",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  Authorization:
                    "Basic " +
                    Buffer.from(
                      secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                    ).toString("base64"),
                  "Content-Length": Buffer.byteLength(postData),
                },
              };

              const linkResponse = await performRequest(
                token_options,
                postData
              );
              const jsonResponse = JSON.parse(linkResponse);
              const dropbox_token = await bot.set(
                "dropbox_token",
                jsonResponse.access_token
              );

              // Delete the file again
              let options = {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${dropbox_token}`,
                  "Content-Type": "application/json",
                },
              };

              options.hostname = "api.dropboxapi.com";
              options.path = "/2/files/delete_v2";

              try {
                await performRequest(options, optionsdata);
              } catch (error) {
                console.error("Error deleting file:", error);
                // return;
              }
            }
          } catch (error) {
            console.error("Error deleting file:", error);
            // return;
          }

          const filename = await db.get(
            `${user}_profile_${id}_avatar_filename`
          );
          let old_avatar_filepath = "/bot/";
          old_avatar_filepath += await db.get(
            `${user}_profile_${id}_avatar_path`
          );
          let new_avatar_filepath = `${old_avatar_filepath}/archived/${filename}`;
          old_avatar_filepath += `/${filename}`;
          let moveoptionsdata = JSON.stringify({
            from_path: old_avatar_filepath,
            to_path: new_avatar_filepath,
          });
          let moveoptions = {
            method: "POST",
            headers: {
              Authorization: `Bearer ${dropbox_token}`,
              "Content-Type": "application/json",
            },
          };
          moveoptions.hostname = "api.dropboxapi.com";
          moveoptions.path = "/2/files/move_v2";

          try {
            const linkResponse = await performRequest(
              moveoptions,
              moveoptionsdata
            );
            const jsonResponse = JSON.parse(linkResponse);
            if (
              jsonResponse.error_summary === "expired_access_token/" ||
              jsonResponse.error_summary === "invalid_access_token/"
            ) {
              const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

              const token_options = {
                hostname: "api.dropbox.com",
                path: "/oauth2/token",
                method: "POST",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  Authorization:
                    "Basic " +
                    Buffer.from(
                      secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                    ).toString("base64"),
                  "Content-Length": Buffer.byteLength(postData),
                },
              };

              const linkResponse = await performRequest(
                token_options,
                postData
              );
              const jsonResponse = JSON.parse(linkResponse);
              const dropbox_token = await bot.set(
                "dropbox_token",
                jsonResponse.access_token
              );
              let moveoptions = {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${dropbox_token}`,
                  "Content-Type": "application/json",
                },
              };
              moveoptions.hostname = "api.dropboxapi.com";
              moveoptions.path = "/2/files/move_v2";

              // Move the file again
              try {
                await performRequest(moveoptions, moveoptionsdata);
              } catch (error) {
                console.error("Error moving file:", error);
                // return;
              }
            }
          } catch (error) {
            console.error("Error moving file:", error);
            if (error.code === "ECONNREFUSED") {
              try {
                await wait(500);
                const linkResponse = await performRequest(
                  moveoptions,
                  moveoptionsdata
                );

                const jsonResponse = JSON.parse(linkResponse);
              } catch (error) {
                console.error("Error moving file:", error);
                // return;
              }
            }
            // return;
          }

          let uploadOptions = {
            method: "POST",
            headers: {
              Authorization: `Bearer ${dropbox_token}`,
              "Dropbox-API-Arg": JSON.stringify({
                path: `/bot/${avatar_filepath}`,
                mode: "overwrite",
                autorename: false,
                mute: false,
                strict_conflict: false,
              }),
              "Content-Type": "application/octet-stream",
            },
          };

          uploadOptions.hostname = "content.dropboxapi.com";
          uploadOptions.path = "/2/files/upload";

          try {
            await performRequest(uploadOptions, buffer);
          } catch (error) {
            console.error("Error uploading file:", error);
            return;
          }
          const linkData = JSON.stringify({
            path: `/bot/${avatar_filepath}`,
            settings: {
              access: "viewer",
              allow_download: true,
              audience: "public",
              requested_visibility: "public",
            },
          });

          let linkOptions = {
            hostname: "api.dropboxapi.com",
            path: "/2/sharing/create_shared_link_with_settings",
            method: "POST",
            headers: {
              Authorization: `Bearer ${dropbox_token}`,
              "Content-Type": "application/json",
              "Content-Length": Buffer.byteLength(linkData),
            },
          };
          try {
            let directLink;
            const linkResponse = await performRequest(linkOptions, linkData);
            const jsonResponse = JSON.parse(linkResponse);
            if (jsonResponse.url)
              directLink = jsonResponse.url.replace("&dl=0", "&dl=1");
            if (
              jsonResponse.error_summary === "expired_access_token/" ||
              jsonResponse.error_summary === "invalid_access_token/"
            ) {
              const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

              const options = {
                hostname: "api.dropbox.com",
                path: "/oauth2/token",
                method: "POST",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                  Authorization:
                    "Basic " +
                    Buffer.from(
                      secret.dropbox_app_key + ":" + secret.dropbox_app_secret
                    ).toString("base64"),
                  "Content-Length": Buffer.byteLength(postData),
                },
              };

              const linkResponse = await performRequest(options, postData);
              const jsonResponse = JSON.parse(linkResponse);
              const dropbox_token = await bot.set(
                "dropbox_token",
                jsonResponse.access_token
              );

              // Re-upload the file
              let uploadOptions = {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${dropbox_token}`,
                  "Dropbox-API-Arg": JSON.stringify({
                    path: `/bot/${avatar_filepath}`,
                    mode: "overwrite",
                    autorename: true,
                    mute: false,
                    strict_conflict: false,
                  }),
                  "Content-Type": "application/octet-stream",
                },
              };

              uploadOptions.hostname = "content.dropboxapi.com";
              uploadOptions.path = "/2/files/upload";

              try {
                await performRequest(uploadOptions, buffer);
              } catch (error) {
                console.error("Error uploading file:", error);
                return;
              }
              const linkData = JSON.stringify({
                path: `/bot/${avatar_filepath}`,
                settings: {
                  access: "viewer",
                  allow_download: true,
                  audience: "public",
                  requested_visibility: "public",
                },
              });

              let linkOptions = {
                hostname: "api.dropboxapi.com",
                path: "/2/sharing/create_shared_link_with_settings",
                method: "POST",
                headers: {
                  Authorization: `Bearer ${dropbox_token}`,
                  "Content-Type": "application/json",
                  "Content-Length": Buffer.byteLength(linkData),
                },
              };
              try {
                const linkResponse = await performRequest(
                  linkOptions,
                  linkData
                );
                const jsonResponse = JSON.parse(linkResponse);

                directLink = jsonResponse.url.replace("&dl=0", "&dl=1");
                return directLink;
              } catch (error) {
                console.error("Error creating shared link:", error);
              }
            }
            if (
              jsonResponse.error_summary === "expired_access_token/" ||
              jsonResponse.error_summary === "invalid_access_token/"
            )
              return console.log(
                "Bot token has expired. Please get the app auth code with the following url.\n",
                `https://www.dropbox.com/oauth2/authorize?client_id=${secret.dropbox_app_key}&response_type=code&token_access_type=offline`,
                "\nAfter getting the auth code, store it in ``secret.dropbox_refresh_token``"
              );
            return directLink;
          } catch (error) {
            console.error("Error creating shared link:", error);
          }
        }

        // Fetch and upload avatar
        async function fetchavatar(user, id, avatarUrl, secret, db) {
          try {
            const response = await fetch(avatarUrl);
            if (!response.ok) {
              throw new Error("Failed to fetch URL.");
            }

            const contentType = response.headers.get("content-type");
            if (!contentType || !contentType.startsWith("image")) {
              throw new Error("Not a valid image URL.");
            }

            const avatarExt = contentType.split("/")[1];
            const path = `avatar/${user}/${id}`;
            if (!fs.existsSync(path)) {
              fs.mkdirSync(path, { recursive: true });
            }
            const avatarFilePath = `${path}/avatar.${avatarExt}`;

            // Stream the image to a file
            const fileStream = fs.createWriteStream(avatarFilePath);
            response.body.pipe(fileStream);

            await new Promise((resolve, reject) => {
              fileStream.on("finish", resolve);
              fileStream.on("error", reject);
            });

            // Read the file into a buffer
            const buffer = fs.readFileSync(avatarFilePath);

            const link = await uploadAndGetLink(
              avatarFilePath,
              buffer,
              secret,
              user,
              id
            );

            await db.set(`${user}_profile_${id}_avatar`, link);
            return link;
          } catch (error) {
            console.error("Error:", error);
            interaction.editReply({
              content: `Error: ${error.message}`,
              ephemeral: true,
            });
            return null;
          }
        }

        async function fetchAvatarUrl(profile_copy_from, guild, user) {
          return new Promise((resolve, reject) => {
            let options = {
              method: "GET",
              headers: {
                Authorization: `Bot ${secret.token}`,
              },
              hostname: "discord.com",
              path: `/api/guilds/${guild}/members/${profile_copy_from}`,
            };

            const req = https.request(options, (res) => {
              res.setEncoding("utf8");
              let val = "";
              res.on("data", (chunk) => {
                val += chunk;
              });
              res.on("end", () => {
                try {
                  const data = JSON.parse(val);
                  let avatar_ext;
                  if (data.avatar)
                    avatar_ext = data.avatar.startsWith("a_") ? "gif" : "webp";
                  let url = `https://cdn.discordapp.com/guilds/${guild}/users/${profile_copy_from}/avatars/${data.avatar}.${avatar_ext}`;
                  let avatar_url = data.avatar ? url : user.displayAvatarURL();
                  resolve(avatar_url);
                } catch (error) {
                  reject(error);
                }
              });
            });

            req.on("error", (e) => {
              reject(e);
            });

            req.end();
          });
        }
        let profile_avatar;
        if (profile_copy_from)
          profile_avatar = await fetchAvatarUrl(
            profile_copy_from.user,
            profile_copy_from.guild,
            user
          );

        let avatar_status;
        if (profile_avatar)
          avatar_status = await fetchavatar(
            interaction.member.id,
            modify_profile_id,
            profile_avatar,
            secret,
            db
          );

        // Give the bot 5 second incase the token expired
        if (profile_avatar && !avatar_status) await wait(5000);
        if (profile_avatar && !avatar_status)
          return interaction.editReply(
            `Failed to update profile \`\`${modify_profile_id}\`\``
          );

        str += `\nSuccessfully updated profile \`\`${modify_profile_id}\`\``;
        str += `\nProfile Name: \`\`${profile_name}\`\``;

        interaction.editReply({ content: str });
      }
    } catch (error) {
      console.error(`Error executing profile (profile) command:`, error);
    }
  },
};

async function uploadAndGetLink(avatar_filepath, buffer, secret) {
  const performRequest = (options, data = null) => {
    return new Promise((resolve, reject) => {
      const req = https.request(options, (res) => {
        let response = "";
        res.on("data", (chunk) => {
          response += chunk;
        });
        res.on("end", () => {
          resolve(response);
        });
      });
      req.on("error", reject);
      if (data) {
        req.write(data);
      }
      req.end();
    });
  };

  const dropbox_token = await bot.get("dropbox_token");
  let uploadOptions = {
    method: "POST",
    headers: {
      Authorization: `Bearer ${dropbox_token}`,
      "Dropbox-API-Arg": JSON.stringify({
        path: `/bot/${avatar_filepath}`,
        mode: "overwrite",
        autorename: true,
        mute: false,
        strict_conflict: false,
      }),
      "Content-Type": "application/octet-stream",
    },
  };

  uploadOptions.hostname = "content.dropboxapi.com";
  uploadOptions.path = "/2/files/upload";

  try {
    await performRequest(uploadOptions, buffer);
  } catch (error) {
    console.error("Error uploading file:", error);
    return;
  }
  const linkData = JSON.stringify({
    path: `/bot/${avatar_filepath}`,
    settings: {
      access: "viewer",
      allow_download: true,
      audience: "public",
      requested_visibility: "public",
    },
  });

  let linkOptions = {
    hostname: "api.dropboxapi.com",
    path: "/2/sharing/create_shared_link_with_settings",
    method: "POST",
    headers: {
      Authorization: `Bearer ${dropbox_token}`,
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(linkData),
    },
  };
  try {
    let directLink;
    const linkResponse = await performRequest(linkOptions, linkData);
    const jsonResponse = JSON.parse(linkResponse);
    if (jsonResponse.url)
      directLink = jsonResponse.url.replace("&dl=0", "&dl=1");
    if (
      jsonResponse.error_summary === "expired_access_token/" ||
      jsonResponse.error_summary === "invalid_access_token/"
    ) {
      const postData = `grant_type=refresh_token&refresh_token=${secret.dropbox_refresh_token}`;

      const options = {
        hostname: "api.dropbox.com",
        path: "/oauth2/token",
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Basic " +
            Buffer.from(
              secret.dropbox_app_key + ":" + secret.dropbox_app_secret
            ).toString("base64"),
          "Content-Length": Buffer.byteLength(postData),
        },
      };

      const linkResponse = await performRequest(options, postData);
      const jsonResponse = JSON.parse(linkResponse);
      const dropbox_token = await bot.set(
        "dropbox_token",
        jsonResponse.access_token
      );

      // Re-upload the file
      let uploadOptions = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${dropbox_token}`,
          "Dropbox-API-Arg": JSON.stringify({
            path: `/bot/${avatar_filepath}`,
            mode: "overwrite",
            autorename: true,
            mute: false,
            strict_conflict: false,
          }),
          "Content-Type": "application/octet-stream",
        },
      };

      uploadOptions.hostname = "content.dropboxapi.com";
      uploadOptions.path = "/2/files/upload";

      try {
        await performRequest(uploadOptions, buffer);
      } catch (error) {
        console.error("Error uploading file:", error);
        return;
      }
      const linkData = JSON.stringify({
        path: `/bot/${avatar_filepath}`,
        settings: {
          access: "viewer",
          allow_download: true,
          audience: "public",
          requested_visibility: "public",
        },
      });

      let linkOptions = {
        hostname: "api.dropboxapi.com",
        path: "/2/sharing/create_shared_link_with_settings",
        method: "POST",
        headers: {
          Authorization: `Bearer ${dropbox_token}`,
          "Content-Type": "application/json",
          "Content-Length": Buffer.byteLength(linkData),
        },
      };
      try {
        const linkResponse = await performRequest(linkOptions, linkData);
        const jsonResponse = JSON.parse(linkResponse);

        directLink = jsonResponse.url.replace("&dl=0", "&dl=1");
        return directLink;
      } catch (error) {
        console.error("Error creating shared link:", error);
      }
    }
    if (
      jsonResponse.error_summary === "expired_access_token/" ||
      jsonResponse.error_summary === "invalid_access_token/"
    )
      return console.log(
        "Bot token has expired. Please get the app auth code with the following url.\n",
        `https://www.dropbox.com/oauth2/authorize?client_id=${secret.dropbox_app_key}&response_type=code&token_access_type=offline`,
        "\nAfter getting the auth code, store it in ``secret.dropbox_refresh_token``"
      );
    return directLink;
  } catch (error) {
    console.error("Error creating shared link:", error);
  }
}

// Fetch and upload avatar
async function fetchavatar(interaction, id, avatarUrl, secret, db) {
  try {
    const response = await fetch(avatarUrl);
    if (!response.ok) {
      throw new Error("Failed to fetch URL.");
    }

    const contentType = response.headers.get("content-type");
    if (!contentType || !contentType.startsWith("image")) {
      throw new Error("Not a valid image URL.");
    }

    const avatarExt = contentType.split("/")[1];
    const path = `avatar/${interaction.member.id}/${id}`;
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }
    const avatarFilePath = `${path}/avatar.${avatarExt}`;

    // Stream the image to a file
    const fileStream = fs.createWriteStream(avatarFilePath);
    response.body.pipe(fileStream);

    await new Promise((resolve, reject) => {
      fileStream.on("finish", resolve);
      fileStream.on("error", reject);
    });

    // Read the file into a buffer
    const buffer = fs.readFileSync(avatarFilePath);

    const link = await uploadAndGetLink(avatarFilePath, buffer, secret);

    await db.set(`${interaction.member.id}_profile_${id}_avatar`, link);
    await db.set(`${interaction.member.id}_profile_${id}_avatar_path`, path);
    await db.set(
      `${interaction.member.id}_profile_${id}_avatar_filename`,
      `avatar.${avatarExt}`
    );
    return true;
  } catch (error) {
    console.error("Error:", error);
    interaction.editReply({
      content: `Error: ${error.message}`,
      ephemeral: true,
    });
    return false;
  }
}
