const {
  PermissionsBitField,
  ApplicationCommandOptionType,
  ButtonStyle,
} = require("discord.js");
const { QuickDB } = require("quick.db");
const db = new QuickDB({ filePath: "database/server.sqlite" });

module.exports = {
  data: {
    name: "webhook",
    description: "[WIP]Resend all your message as webhook",
    options: [
      {
        name: "enable",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Enable resending message as webhook",
        options: [],
      },
      {
        name: "disable",
        type: ApplicationCommandOptionType.Subcommand,
        description: "Send message normally",
        options: [],
      },
    ],
    trans: "webhook",
  },
  async execute(client, interaction, args, secret, trans, langc, guild) {
    try {

      const status = interaction.options.getSubcommand();
      const now = await db.get(`webhook_${interaction.channel.id}`);

      if (status === "enable") {
        if (now && JSON.stringify(now).includes(interaction.member.id)) {
          const alr_enabled = trans.strings.find(
            (it) => it.name === "alr_enabled"
          ).trans;
          return interaction.reply({ content: alr_enabled, ephemeral: true });
        }

        await db.push(
          `webhook_${interaction.channel.id}`,
          interaction.member.id
        );

        const enabled = trans.strings.find((it) => it.name === "enabled").trans;
        interaction.reply({ content: enabled, ephemeral: true });
      } else {
        if (now && !JSON.stringify(now).includes(interaction.member.id)) {
          const alr_disabled = trans.strings.find(
            (it) => it.name === "alr_disabled"
          ).trans;
          return interaction.reply({ content: alr_disabled, ephemeral: true });
        }

        await db.pull(
          `webhook_${interaction.channel.id}`,
          interaction.member.id
        );

        const disabled = trans.strings.find(
          (it) => it.name === "disabled"
        ).trans;
        interaction.reply({ content: disabled, ephemeral: true });
      }
    } catch (error) {
      console.error(`Error executing webhook (webhook) command: ${error}`);
    }
  },
};
