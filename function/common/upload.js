const createReadStream = require("fs").createReadStream;
const fs = require("fs");
const { google } = require("googleapis");

async function upload(secret, filename) {
  const SCOPES = ["https://www.googleapis.com/auth/drive.file"];
  if (!fs.existsSync(filename)) return "file not found!";

  /**
   * Authorize with service account and get jwt client
   *
   */
  async function authorize() {
    const jwtClient = new google.auth.JWT(
      secret.google_drive_client_email,
      null,
      secret.google_drive_private_key,
      SCOPES
    );
    await jwtClient.authorize();
    return jwtClient;
  }

  try {
    const authClient = await authorize();
    const webViewLink = await uploadFile(authClient, filename);
    return webViewLink;
  } catch (error) {
    console.error(error);
    return null;
  }
}

async function uploadFile(authClient, filename) {
  try {
    const drive = google.drive({ version: "v3", auth: authClient });
    const file = await drive.files.create({
      media: {
        body: createReadStream(filename),
      },
      fields: "id",
      requestBody: {
        name: `bot/${filename.split("/")[1]}`,
      },
    });
    // console.log(file.data.id);

    await drive.permissions.create({
      fileId: file.data.id,
      requestBody: {
        role: "reader",
        type: "anyone",
      },
    });

    const fileGetResponse = await drive.files.get({
      fileId: file.data.id,
      fields: "webViewLink",
    });

    const webViewLink = fileGetResponse.data.webViewLink;
    //   console.log(webViewLink);
    return webViewLink;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

module.exports = upload;
